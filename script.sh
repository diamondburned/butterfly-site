#!/bin/bash
IP="23.226.136.11"
PORT="22"
USER=$(whoami)

rsync --delete --progress -e "ssh -p $PORT" -r public/ "${USER}@${IP}:/home/${USER}/public/"
