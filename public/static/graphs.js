var grafana = "https://butterfly.diamondb.xyz/grafana/d-solo/ufOJ2RFiz/public-butterfly-bot?orgId=1&panelId=";
var grafana_token = "eyJrIjoiM3ZpQ1RtU1cydEh1ZEh4T3VJQTFESWRPaW96YWp3WFQiLCJuIjoid2Vic2l0ZSIsImlkIjoxfQ==";

var panels = new Array()
	panels['guilds'] = 4
	panels['users'] = 10
	panels['commands'] = 2

function getgraph(url, divid) {
	var request = new XMLHttpRequest();
	request.onreadystatechange = (function() {
		if (this.readyState == 4 && this.status == 200) {
			var iframe = this.responseText;
			document.querySelector(`iframe.${divid}`).srcdoc = iframe;
		}
	});
	request.open("GET", url);
	console.log(url);
	request.setRequestHeader('Authorization', 'Bearer ' + grafana_token);
	request.send();
}

for (var panel in panels) {
	var url = `${grafana}${panels[panel]}`;
	getgraph(url, panel);
}
