---
title: "/np/ Now playing, for showing off or just easier sharing"
date: 2018-08-01T23:35:11+07:00
anchor: "nowplaying"
weight: 3
---

Idea from the `np` command in osu!

- Usage: `~np`
- Supported integrations (as of Aug 2): osu!, Spotify

### osu!
![osu!](images/screenshots/nowplaying/osu.png)

### Spotify
![Spotify](images/screenshots/nowplaying/spotify.png)