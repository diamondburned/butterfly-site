---
title: "/calc/ Calculator"
date: 2018-08-01T23:35:10+07:00
anchor: "calculator"
weight: 8
---

- Usage: `~calc [equation]`
	- Note: `~calc` accepts `bc -l` or the standard math library.
- Example: `~calc 4*a(1)` <font color="gray"># calculates Pi</font>

### Screenshot

![calc](images/screenshots/calc.png)