---
title: "/kitsu/ Kitsu related commands, for looking up anime and manga"
date: 2018-08-01T23:35:11+07:00
anchor: "kitsu"
weight: 2
---

- `~kitsu`: Find anime and manga from Kitsu.io 
	- Usage: `~kitsu [OPTIONS] [keywords]` finds anime
		- Options (optional, falls back to `anime`)
			- `anime` - finds anime, default behavior
			- `manga` - finds manga
			- `search [anime|manga] - list search entries instead of information`
	- Example: `~kitsu Arifureta`

### Screenshot

![Kitsu](images/screenshots/kitsu.png)