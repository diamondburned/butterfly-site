---
title: "/saucenao/ Saucenao, to reverse lookup images in server"
date: 2018-08-01T23:35:10+07:00
anchor: "saucenao"
weight: 6
---

- Usage: `~saucenao [link, optional]`
	- Note: if no link is supplied, `~saucenao` must be the content of a file uploaded in Discord
	- The rate limit is 1 command per minute, with a global limit of 30 commands per 30 seconds. ~~Blame Saucenao for this.~~

### Screenshots

![Saucenao](images/screenshots/saucenao.png)