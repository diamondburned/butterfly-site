---
title: "/time/ Time"
date: 2018-08-01T23:35:10+07:00
anchor: "time"
weight: 7
---

- Querying
    - Usage: `~time [city]`
    - Example: `~time Los Angeles`
- Setting default time for current user
    - Usage: `~time [-s|--set] [Linux Timezone]`
        - `Linux Timezone` can be found by running `~time [city]`
        - Time set will soon be used throughout the bot, such as in printed dates, etc.
    - Example: `~time -s America/Los_Angeles`

### Screenshot

![time](images/screenshots/time.png)
