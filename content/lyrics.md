---
title: "/lyrics/ Genius, for when you need to look up lyrics"
date: 2018-08-01T23:35:10+07:00
anchor: "lyrics"
weight: 10
---

~~Or maybe sing Despacito, who knows?~~

- `~lyrics`
	- `~lyrics [keywords]` searches Genius for IDs
	- `~lyrics id [id]` queries Genius for Lyrics from ID

### Step 1 - `~lyrics Black Rover`

![search](images/screenshots/lyrics/search.png)

### Step 2 - `~lyrics id 3800133`

![id](images/screenshots/lyrics/id.png)
