---
title: "/twitch/ To query Twitch streamers"
date: 2018-08-11T12:49:55+07:00
anchor: "twitch"
weight: 7.5
---

- Usage: `~twitch [username]`
- Example: `~twitch thepoon02`

### Screenshot

![twitch](/images/screenshots/twitch.png)