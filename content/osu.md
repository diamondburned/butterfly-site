---
title: "/osu/ osu! related commands, including beatmap search"
date: 2018-08-01T23:35:11+07:00
anchor: "osu"
weight: 1
---

- `~osuset` - sets your username
	- Usage: `osuset [options] [username]`
		- Options (optional)
			- `[-n|--nickname] [nickname]` sets your nickname/alias (you have to be voted to use this)
			- `[-r|--ripple]` sets your Ripple username
- `~osu` - prints profile statistics and information
	- Possible commands: `~osu` | `~mania` | `~taiko` | `~ctb`
	- Posssible usages:
		- `~osu [Options]` returns the profile for the username you set
		- `~osu [Options] [@mention]` returns the profile for the player you mentioned
		- `~osu [Options] [username]` returns the profile for that username
			- Options (optional)
				- `--ripple` queries your Ripple profile 
				- `[-d|--details]` returns extra information
- `~osutop` prints profile statistics and information
	- Possible commands: 
		- `~osutop` | `~maniatop` | `~taikotop` | `~ctbtop`
		- `~osutop` returns the top (3) plays
		- `~osutop [username|@mention]` returns the top (3) plays for that user
		- `~osutop [Options] [username|@mention]` returns the top `n` plays for that user (n from 1 to 10)
			- Options (optional)
				- `[-s|--shift] [1..20]` - sets the page (1 would show your most recent (default), 2 would show your second page, ...)
	- Example:
		- `~osutop -s 2` would show your 5th to 10th top play
-  `~recent` | `~r` prints the most recent plays and its statistics
	- Usage: `~recent [options] [username|@mention]` or `~recent [options]`
		- Options (optional)
			- `--ripple` queries your Ripple profile
			- `[-m|--mode] [std|mania|taiko|ctb]` changes mode (default `std`)
			- `[-s|--shift] [1..10]` sets the page
			- `--angle` calculates Xexxar's proposed changes
	- Example:
		- `~r -s 5` prints your 5th most recent play
		- `~recent cookiezi` prints cookiezi™'s most recent play
- `~score` finds your top play and print it
	- Usage: `~score [beatmap link] [Options]`
		- Options (optional)
			- `[username|@mention]` - the user you want to query
			- `[-l|--list] [1..10]` lists n entries instead of default 1
			- `[-s|--shift] [1..n]` shifts to the nth entry
			- `--angle` calculates Xexxar's proposed changes
- `~compare` fetches bot messages and sort them in a list to easily compare scores. 
	- Only works with outputs with 1 score only, such as `~recent` or `~score` (1 entry/score)
- `~pp` calculates pp for a beatmap
	- Usage: `~pp [link to beatmap/beatmapset] [OPTIONS]`
		- Options (optional)
			- `+[HD|HR|...]` - include mods into the formula
			- `[1..100]%` - include accuracy into the formula
			- `[1..n]x` - include combo into the formula
			- `--angle` calculates Xexxar's proposed changes
- `~mapsearch`: Show help for looking up maps
	- Usage: `~mapsearch title MAPTITLE | [Options]`
		- Options (marked with `!/` are arguments in which at least one is needed)
			- `!/` `title` - search with map title
			- `!/` `artist` - search with map artist
			- `!/` `mapper` - search with map mapper
			- `!/` `diffname` - search with map difficulty name
			- `exact` - show only exact matches
			- `standard` - show only osu!standard maps
			- `mania` - show only osu!mania maps
			- `taiko` - show only osu!taiko maps
			- `ctb` - show only osu!ctb maps
			- `ranked` - show only ranked maps
			- `qualified` - show only qualified maps
			- `loved` - show only loved maps
	- Example: `~mapsearch title Jitter Doll | mapper -Laura- | ranked`

### `~osu`

![osu](images/screenshots/osu/osu.png)

### `~osutop`

![top](images/screenshots/osu/top.png)