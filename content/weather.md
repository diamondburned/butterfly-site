---
title: "/wttr/ wttr.in, a simple weather report site"
date: 2018-08-01T23:35:10+07:00
anchor: "weather"
weight: 9
---

### Work in Progress, might change in the future

- `~wttr [OPTIONS] [city]` finds current weather
	- Options (optional)
		- `-c` reports the temperature in Celcius
		- `-f` reports the temperature in Fahrenheit