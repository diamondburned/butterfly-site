---
title: "/sourcequery/ Sourcequery, queries most Source servers"
date: 2018-08-01T23:35:11+07:00
anchor: "sourcequery"
weight: 5
---

- Usage: `~sourcequery [IP:port]`
- If you're voted, a player list should show up

### Screenshot

![Sourcequery](images/screenshots/sourcequery/def.png)

### Voted

![Sourcequery Voted](images/screenshots/sourcequery/voted.png)
