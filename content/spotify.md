---
title: "/spotify/ Spotify, for when you want to look up songs"
date: 2018-08-01T23:35:10+07:00
anchor: "spotify"
weight: 5
---

- Usage: `~spotify`
	- First case: `~spotify [track|artist|album] [keyword]`
	- Second case: `~spotify [trackid] [keyword]`
		- Track ID can be get from the tail of a `track` URL: 
			- `https://open.spotify.com/track/3YRCqOhFifThpSRFJ1VWFM`
				- Track URL because it has `https://open.spotify.com/track/`
				- Its ID is `3YRCqOhFifThpSRFJ1VWFM`
- Example: `~spotify track November Rain`

### `~spotify track November Rain`

![Spotify](images/screenshots/spotify/spotify.png)

### `~spotify trackid 3YRCqOhFifThpSRFJ1VWFM`

![Spotify2](images/screenshots/spotify/spotifyid.png)
