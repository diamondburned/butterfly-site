---
title: "/arch/ Arch Linux related commands, for looking up AUR or Repository packages"
date: 2018-08-01T23:37:51+07:00
anchor: "arch-linux"
weight: 4
---

- `~aur` finds packages on the Arch Linux User Repository
	- Usage: `~aur [OPTIONS] [keywords]`
		- Options (optional)
			- `[-e|--exact]` - exact match only
- `~packages` finds packages on the Arch Linux Repository
	- Usage: `~packages [OPTIONS] [keywords]`
		- Options (optional)
			- `[-e|--exact]` - exact match only

### Screenshot

![aur](images/screenshots/aur.png)