---
title: "/vote/ Voting for the bot"
date: 2018-09-22T17:56:02-07:00
anchor: "vote"
weight: 1
---

- Benefits of Voting
    - A thank you message when you do stuff
    - Setting nicknames for `~osu` and osu-related commands
    - There's more but I don't remember it all...
- How to vote:
    - Run `~hasVoted`
    - Vote at [discordbots.org/bot/463221705628975104](https://discordbots.org/bot/463221705628975104)

### Nickname

![time](images/screenshots/vote.png)
