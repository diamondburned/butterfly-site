---
title: "/miscellaneous/ Toggle and Random Choice Generator"
date: 2018-08-01T23:35:12+07:00
anchor: "misc"
weight: 99
---

- `~toggle [feature]` shows a list of toggleable features
	- `despacito` toggles a response for `des` and `cito`
	- `hentai` toggles n/ehentai link parsing (default: `true`)
	- `osulink` toggles osu! beatmap link parsing (default: `true`)
- `~choose [choices]` chooses randomly for you
	- Delimiter is the word `or`
	- ~~praise rngesus~~
